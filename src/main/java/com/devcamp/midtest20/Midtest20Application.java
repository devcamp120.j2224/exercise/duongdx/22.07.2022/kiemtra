package com.devcamp.midtest20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Midtest20Application {

	public static void main(String[] args) {
		SpringApplication.run(Midtest20Application.class, args);
	}

}

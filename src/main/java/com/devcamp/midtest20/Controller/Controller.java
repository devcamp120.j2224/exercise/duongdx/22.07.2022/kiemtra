package com.devcamp.midtest20.Controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin
public class Controller {
    @GetMapping("/StringAPI")
    public String getStringApi(@RequestParam("String") String string){
        StringBuilder str = new StringBuilder(string);
        String result = str.reverse().toString() ;
        return result ;
    }

    @GetMapping("/palindrome")
    public String getPalindromeApi(@RequestParam("String") String string){
        String Result = "";
        StringBuilder stringBuilder = new StringBuilder(string);
        if(string.equals(stringBuilder.reverse().toString()) == true){
            Result = "Đây Là chuỗi Đảo Ngược";
        }else{ 
            Result = "Đây không phải là chuỗi Đảo ngược";
        }
        return Result ;
    }

  
    @GetMapping("/findNonRepeat")
    public String getfindNonRepeatApi(@RequestParam("String") String str){
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < str.length(); i++) {
            if (!charsPresent.contains(str.charAt(i))) {
                stringBuilder.append(str.charAt(i));
                charsPresent.add(str.charAt(i));
            }
        }

        return stringBuilder.toString();
    }

    @GetMapping("Appendstringtogether")
    public String minCat(@RequestParam(value="str1")String str1, 
                         @RequestParam(value="str2")String str2)
    {
        if (str1.length() == str2.length())
            return str1+str2;
        if (str1.length() > str2.length())
        {
            int diff = str1.length() - str2.length();
            return str1.substring(diff, str1.length()) + str2;
        } else
        {
            int diff = str2.length() - str1.length();
            return str1 + str2.substring(diff, str2.length());
        }
    }
}
